
/**************************************************
  
  // Ring Interrupt
  #define RingInt 0  
  #define PulsInt 1  
 
  
  
  Total
  Input Output
  6      5

  2 for Serial
  
  Pin Details

  Serial RX A1        // Input
  Serial TX A2        // Output

  #define RingPin  2  // Input
  #define RSTPIN  12  // Output

  // Pin Detials
  #define PulsePin   3  // Input
  #define SQUAREWAVE 4  // Output

  //Motor Pin 
  #define MOTOR_EN 5  // Output
  #define MOTOR1   6  // Output
  #define MOTOR2   7  // Output

  //Set Limit for Valve
  #define OPENLIMITPIN  8 // Input
  #define CLOSELIMITPIN 9 // Input


**************************************************/

#include <avr/sleep.h>

#include "CommonDefn.h"
#include "TimerLibry.h"
#include "MotorControl.h"
#include "GSMLibry.h"
#include "MQTTLibry.h"

const uint8_t ConnectCont = 20;

uint8_t bTCPMux;
volatile uint8_t bDataUpdate = 0;

uint32_t PrevTime;
uint32_t CurTime;
boolean ledState;

#define MinCount 1
const uint32_t WakeUpTime = (60000 * MinCount); 
uint8_t bRecvCmd;

void setup()
{
  uint8_t PinIndex;
  
  // put your setup code here, to run once
  Serial.begin(9600);

  Serial.println(F("SMART WATER METER (V1.00)"));

  delay(1000);
  
  noInterrupts();           // disable all interrupts

// Unused pins
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(13, OUTPUT);

  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  
  InitTimer(); 
  Serial.println(F("Timer Initialize"));
  delay(100);
  
  InitMotor();  
  Serial.println(F("Motor Initialize"));
  delay(100);
  
  InitGSM();
  Serial.println(F("GSM Initialize"));
  delay(100);
  
  InitMQTT();
  Serial.println(F("MQTT Initialize"));
  delay(100);  

  TCPConCntr  = 0;

// enable all interrupts    
  interrupts();

  Serial.println(F("Resetting Modem"));

  RST_GSM ();
  DelayFun();

  CIPSHUT();

  bDataUpdate = 0;

//  DeepSleepFunc();
  ADCSRA &= ~(1 << 7);

  bRecvCmd = 1;
}

//void loop()
//{
////  CheckMotor();
////  MotorControl(FWD);
//  MotorControl(STP);
//
//  Going_To_Sleep();
//}


void loop()
{
  static char Val = 0;
  // put your main code here, to run repeatedly:
//  uint8_t ValveSts;

  
    
//  ValveStatus();
//  ValveControl();
   
  if(TCPConCntr < ConnectCont)
    bDataUpdate = 1;
  else
  {
    InitTCP();

    
      
  }
          
        
 // if((ValveSts != OPENED) && (ValveSts != CLOSED)) 
  if(bTCPConnected == 1)
  {
    if(BIP_State == _IP_CONNECT)
    {
      if(bMQTTConct == 0)
      {
        bMQTTConct = SendMQTTPacket(_MQTT_CON);
        SendATCommand(AT_CIPRXGET2, 2000);

        if(bMQTTConct == 1)
        {
          CurTime = millis();
        }
      }
      else if(bMQTTConct == 1)
      {
        
        SendMQTTPacket(_MQTT_PUB_01);

//        SendMQTTPacket(_MQTT_PUB_02);

        do
        {
          if(SendMQTTPacket(_MQTT_SUB_01) == 1)
            Val = SendATCommand4(AT_CIPRXGET2, 5000);
//          else
//            break;
          
  
          Serial.print(F("In main => "));
          Serial.write(Val + 0x30);
          Serial.println();
       
          if(Val == 2)
          {
            ValCommand = CLOSE;
          }
          else if(Val == 1) 
          {
            ValCommand = OPEN;
          }
   
          if(Val != 0)
          {
//            bDataUpdate = 1;
            bRecvCmd = 1;
            break;
          }
        }while(((millis() - CurTime) < WakeUpTime));

        if(bRecvCmd == 0)
          bDataUpdate = 1;  
      }
        
    }
  }


  if(bRecvCmd == 1)
  {
    if(ValCommand == OPEN)
    {
      if(ValveSts == OPENED)
      {
        bDataUpdate = 1;
        Serial.println(F("11"));
      }
      else
        Serial.println(F("12"));     
     }
     else if(ValCommand == CLOSE)
     {
      if(ValveSts == CLOSED)
      {
        bDataUpdate = 1;
        Serial.println(F("21"));
      }
      else
        Serial.println(F("22"));
     }
  }
      
        
  
  if(bToggle == 0)
  {
    EEPROM.put(Pulse_ADR, (uint32_t)PulseCounter);
    PulseToLiter(); 

    PrevPulseCounter = PulseCounter;
    
    
    Serial.print(F("PulseCounter => "));
    Serial.println(PulseCounter);
    
    Serial.print(F("Totaliser => "));
    Serial.println(Totaliser);
    
    Serial.print(F("TCPConCntr => "));
    Serial.println(TCPConCntr);    

    Serial.print(F("ValCommand => "));

    if(ValCommand == OPEN)
      Serial.println("OPEN");
    else if(ValCommand == CLOSE)
      Serial.println("CLOSE");
    
    Serial.print("bDataUpdate => ");
    Serial.println(bDataUpdate);

    Serial.print("ValveSts => ");
    Serial.println(ValveSts);

    bToggle = 1;
  }
    
  if((ValveSts == OPENED) || (ValveSts == CLOSED))
  if(bDataUpdate == 1)
  {
    bRecvCmd = 0;
    bDataUpdate = 0;
    MotorControl(STP);
    
    if(TCPConCntr >= ConnectCont)
      TCPConCntr = 0;
      
    CIPSHUT();
    BIP_State = _IP_STS;
    Going_To_Sleep();  
  }
  
  if(WakeUpFlag == 1)
  {
    WakeUpFlag = 0;
    bRecvCmd = 1;
    ValCommand = OPEN;
     bToggle = 0;
    Serial.println(F("Ring Interrupt detected"));
  }
}


void Going_To_Sleep()
{
  Serial.println(F("\r\nEnter into sleep mode"));
  attachInterrupt(RingInt, WakeUpFunInt, FALLING); 

  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  
  sleep_enable();
  digitalWrite(LED_BUILTIN,LOW);
  
  delay(1000);
  
  //BOD DISABLE - this must be called right before the __asm__ sleep instruction
  MCUCR |= (3 << 5); //set both BODS and BODSE at the same time
  MCUCR = (MCUCR & ~(1 << 5)) | (1 << 6); //then set the BODS bit and clear the BODSE bit at the same time
  
  sleep_cpu();
  
  Serial.println(F("Just woke up"));
  digitalWrite(LED_BUILTIN,HIGH);
  
  GSMCSCLK();
  delay(3000);
}
