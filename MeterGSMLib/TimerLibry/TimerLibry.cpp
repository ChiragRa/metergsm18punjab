

#include "TimerLibry.h"

volatile uint8_t TimeOvr1Sec;
volatile uint32_t TimeCntr;



void InitTimer(void)
{
	SetupTimer1();
	TimeOvr1Sec = 0;
}

void SetupTimer1(void)
{
  TCCR1A = 0;
  TCCR1B = 0;
  TimeCntr = 3036;   // preload timer 65536-16MHz/256/2Hz
  TCNT1 = TimeCntr;   // preload timer
  TCCR1B |= (1 << CS12);    // 256 prescaler 
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow //interrupt  
}

ISR(TIMER1_OVF_vect)        // interrupt service routine 
{
//  static volatile uint8_t WaveCntr = 0;
  
  TCNT1 = TimeCntr;   // preload timer

//  WaveCntr++;
  
 // if(WaveCntr == 3)
  {
    TimeOvr1Sec ^= 1;
 //   WaveCntr = 0;
  }
}

void DelayFun (void)
{
  while(TimeOvr1Sec == 0);
  while(TimeOvr1Sec == 1);
}