

#ifndef  TimerLIBRY_H_
#define TimerLIBRY_H_

#include "CommonDefn.h"

extern volatile uint8_t TimeOvr1Sec;
extern volatile uint32_t TimeCntr;


void InitTimer(void);
void SetupTimer1(void);
void DelayFun (void);

#endif