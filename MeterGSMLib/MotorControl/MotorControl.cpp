

#include "CommonDefn.h"
#include "MotorControl.h"
#include "TimerLibry.h"
#include "GSMLibry.h"


uint8_t OpenLimtSts;
uint8_t CloseLimtSts;

uint8_t ValCommand;

uint32_t Totaliser;
volatile uint32_t PulseCounter;
volatile uint32_t PrevPulseCounter;
volatile uint8_t bToggle;
uint8_t ValveSts;

void PulseCntFunInt(void);

void InitMotor(void)
{
	pinMode(PulsePin,INPUT_PULLUP);

	pinMode(MOTOR1,OUTPUT);
  	pinMode(MOTOR2,OUTPUT);
	pinMode(MOTOR_EN,OUTPUT);

  	pinMode(OPENLIMITPIN,INPUT_PULLUP);
  	pinMode(CLOSELIMITPIN,INPUT_PULLUP);
	
	pinMode(SQUAREWAVE,OUTPUT);

	digitalWrite(SQUAREWAVE,HIGH);

	MotorControl(STP);
	
        ValveSts = CLOSE;  
  	OpenLimtSts = 0;
  	CloseLimtSts = 0;

	ValCommand = OPEN;

	PulseCounter = 0;
  	bToggle = 0;

	EEPROM.get(Pulse_ADR, PulseCounter);
	
	if(PulseCounter > 99999)
	{
	  PulseCounter = 0;	
	  EEPROM.put(Pulse_ADR, PulseCounter);	
	}

	PrevPulseCounter = PulseCounter;

	PulseToLiter();

	attachInterrupt(PulsInt, PulseCntFunInt, FALLING); 
}

// Pulse counter Interrupt function
void PulseCntFunInt(void)
{
  sleep_disable();
  
  bToggle = 0;
  TCPConCntr++;
  if(PulseCounter < 99999)
  {
    	PulseCounter++;
//  	Serial.println(F(PulseCounter));
  }

  if(PulseCounter >= 100000)
    PulseCounter = 0;    
}

void MotorControl(uint8_t Dirct)
{
  if(Dirct == FWD)
  {
    digitalWrite(MOTOR1, HIGH);
    digitalWrite(MOTOR2, LOW);
    digitalWrite(MOTOR_EN, HIGH);
  }
  else if(Dirct == REV)
  {
    
    digitalWrite(MOTOR1, LOW);
    digitalWrite(MOTOR2, HIGH);
    digitalWrite(MOTOR_EN, HIGH);
  }
  else 
  {
    digitalWrite(MOTOR1, HIGH);
    digitalWrite(MOTOR2, HIGH);
    digitalWrite(MOTOR_EN, LOW);
  }
}


void PulseToLiter(void)
{
  Totaliser = PulseCounter * 5;	
}


void ValveControl(void)
{
  if(ValCommand == OPEN)
  {
    if(ValveSts == OPENED)
    {
      MotorControl(STP);  
    }
    else if(ValveSts == CLOSED)
    {
      MotorControl(REV); 
    }
    else if(ValveSts == NOTCLOSE)
    {
      MotorControl(REV);  
    }     
  }
  else if(ValCommand == CLOSE)
  {
    if(ValveSts == OPENED)
    {
      MotorControl(FWD);  
    }
    else if(ValveSts == CLOSED)
    {
      MotorControl(STP); 
    }
    else if(ValveSts == NOTCLOSE)
    {
      MotorControl(FWD);  
    }     
  }
  else
  {
    
  }
}


uint8_t ValveStatus(void)
{
  //uint8_t ValveSts = NOTCLOSE;
  
 
  if(digitalRead(OPENLIMITPIN) == 0)	
  {
    delay(20);
    if(digitalRead(OPENLIMITPIN) == 0)
      OpenLimtSts = 0;
  }
  else
   OpenLimtSts = 1; 

  if(digitalRead(CLOSELIMITPIN) == 0)	
  {
    delay(20);
    if(digitalRead(CLOSELIMITPIN) == 0)
      CloseLimtSts = 0;
  }
  else
   CloseLimtSts = 1; 

//  OpenLimtSts  = digitalRead(OPENLIMITPIN);
//  CloseLimtSts = digitalRead(CLOSELIMITPIN);
 
  if((OpenLimtSts == 0) && (CloseLimtSts == 1))
  {
   Serial.println(F("Door is Opened"));
    ValveSts = OPENED; 
  }
  else if((OpenLimtSts == 1) && (CloseLimtSts == 0))
  {
    Serial.println(F("Door is Closed"));
    ValveSts = CLOSED; 
  }
  else if((OpenLimtSts == 1) && (CloseLimtSts == 1))
  {
    Serial.println(F("Not closed Not opened"));
    ValveSts = NOTCLOSE; 
  }
  else
  {
    ValveSts = NOT_DEF;  
  }
  
//  return(ValveSts);
}

void CheckMotor(void)
{
  ValveStatus();
  
  if(ValCommand == OPEN)
  {
    if(ValveSts == OPENED)
    {
      delay(2000);
      ValCommand = CLOSE;
      MotorControl(FWD); 
      Serial.println(F("12"));
    }
    else
    {
      MotorControl(REV);
      Serial.println(F("11"));
    }
  }
  else if(ValCommand == CLOSE)
  {
    if(ValveSts == CLOSED)
    {
      delay(2000);
      ValCommand = OPEN;
      MotorControl(REV); 
      Serial.println(F("22"));
    }
    else
    {
      MotorControl(FWD);  
      Serial.println(F("21"));  
    }
  }  
}

void Generatesqrwave (void)
{
//   Serial.println();
//  Serial.write(TimeeOvr1Sec+0x30);
  digitalWrite(SQUAREWAVE, TimeOvr1Sec);
//  digitalWrite(LED_BUILTIN, TimeOvr1Sec); 
}